import logging
import datetime
import os

# Informacion sobre los tipos de logs:
# https://docs.python.org/3/library/logging.html


# Definiendo el logger
# Mostrar el nombre del modulo de donde se llama
logger = logging.getLogger(__name__)
# Definir el nivel de log
logger.setLevel(logging.INFO)
# Definir formato, fecha hora, nivel de log, mensaje
formatter = logging.Formatter('%(asctime)s %(levelname)s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
# Nombre del archivo log
file_handler = logging.FileHandler('backup.log')
# Se pasa el formato anteriormente definido
file_handler.setFormatter(formatter)
# Para que al mismo tiempo muestre los resultado por cli
stream_handler = logging.StreamHandler()
# pasandole el mismo formato anteriormente definido
stream_handler.setFormatter(formatter)
# definiendo los Handlers de archivos y cli
logger.addHandler(file_handler)
logger.addHandler(stream_handler)

# funcion para que me de las fechas que necesito para crear las carpetas
def folder_names():
    today = datetime.date.today()
    tyear = today.year
    tmonth = datetime.date(tyear, today.month, 1).strftime('%m %B %Y')
    tday = today.day

    return tyear, tmonth, tday

# asignando las variables con el nombre de las carpetas
year_folder, month_folder, day_folder = folder_names()


file_src = os.path.join(os.environ.get('USERPROFILE'), 'OneDrive\Documentos\FP Web\Proyectos\py-backup\source')
file_dest = os.path.join(os.environ.get('USERPROFILE'), 'OneDrive\Documentos\FP Web\Proyectos\py-backup\destination')
daily_reports = os.path.join(file_dest, str(year_folder), 'Daily', str(month_folder), str(day_folder))


try:
    os.chdir(daily_reports)
    print('folder exist')
except FileNotFoundError:
    os.makedirs(daily_reports)
    os.chdir(daily_reports)
    print('folder created')





# logger.info(f"la fecha es: {year} {month} {day}")